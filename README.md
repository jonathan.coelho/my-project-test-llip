# My Project Test Llip




## Configurando o Projeto 

Abra o projeto e certifique-se de que as connections strings irão apontar para um servidor local do seu SQL Server. Todas eles estão no arquivo .json do projeto TesteTecnico. 

Abra o Package Manager Console (Visual Studio) e selecione o projeto de infrastructure e rode o comando "update-database" (Migrations) para criar a intância do banco de dados e toda a sua estrutura (tabelas, colunas, chaves.. etc.). 

Selecione o projeto Web chamado TesteTecnico como projeto de inicialização no Visual Studio. 

Rode a aplicação e na tela de login selecione a opção "Create New Account" para criar uma nova conta. Preencha todos os dados e em seguida peça para salvar e aguarde os dados seremcadastrados. 

Agora que você possui uma nova conta volte até a tela de login, insira os dados e navegue pelo sistema. 


## Arquitetura do Projeto 

O projeto foi separado em camadas se baseando na arquitetura original do DDD adaptada para o tipo de projeto em questão. A arquitetura foi feita de forma que pudesse ser expandida para um grande projeto sem dificuldades. 

O projeto TesteTecnico faz parte da camada de apresentação enquanto as outras camadas tem seu nome por si só tal qual sua função, como Application sendo a aplicação de serviços do projeto, Domain o domínio cerebral de toda a estrutura e o Infrastructure sendo a parte responsável pelos dados do projeto. O modelo de injeção de dependência foi o mais simples possível, porém o projeto também permite que se expanda mais uma camada para cuidar de todo este processo caso o projeto cresça. 

## Menus do Projeto 

User -> Controle de todos os usuários existentes na aplicação, assim como a quantidade de tarefas designadas aos mesmos.
Projects -> Controle e criação de projetos da aplicação. 
Tasks -> Controle e criação de tarefas da aplicação, assim como relacionamento da mesma com os projetos e os usuários. 
My Tasks -> Controle das tasks do usuário logado no sistema. 

## Considerações Finais 

Fiz o projeto de modo a me preocupar mais em como meu código se organiza do que em aparências e validações em si. Senti esta liberdade ao ler a documentação do projeto de teste, pois entendi que isto (codificação) seria levado mais em conta na hora da avaliação. 

Tenho experiências com tokens JWT, mas tive alguns problemas ao implementa-lo no projeto MVC. Então preferi retirá-lo para que não pudesse bagunçar o código na avaliação. 

No mais, qualquer dúvida sobre o projeto, peço para que entre em contato com o desenvolvedor. 

## Autor 
Jonathan Crístian Ribeiro Coelho