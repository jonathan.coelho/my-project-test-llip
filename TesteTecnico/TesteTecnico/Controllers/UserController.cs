﻿using Application.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace TesteTecnico.Controllers
{
    public class UserController : Controller
    {

        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        public IActionResult Index()
        {
            var list = _userService.ListUsers().ToList();
            return View(list);
        }


    }
}
