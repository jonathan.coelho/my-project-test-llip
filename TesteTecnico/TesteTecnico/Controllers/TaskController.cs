﻿using Application.Interfaces;
using Application.Model;
using Application.Enums;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TesteTecnico.Controllers
{
    public class TaskController : Controller
    {
        private readonly ITaskService _taskService;
        private readonly IUserService _userService;
        private readonly IProjectService _projectService;

        public TaskController(ITaskService taskService, IUserService userService, IProjectService projectService)
        {
            _taskService = taskService;
            _userService = userService;
            _projectService = projectService;
        }

        public IActionResult Index()
        {
            var list = _taskService.ListTasks().ToList();
            return View(list);
        }

        [HttpGet]
        public IActionResult MyTask()
        {
            string userId = HttpContext.Request.Cookies["UserId"];
            var list = _taskService.ListMyTasks(Convert.ToInt64(userId)).ToList();
            return View("Index", list);
        }

        [HttpGet]
        public IActionResult Form()
        {
            LoadViewBags();
            var model = new TaskViewModel();
            return View(model);
        }


        [HttpGet]
        public IActionResult Edit(long id)
        {
            LoadViewBags();
            var model = _taskService.GetById(id);
            return View("Form", model);
        }


        private void LoadViewBags()
        {
            List<ProjectViewModel> listProjectViewModels = _projectService.ListProjects().ToList();
            ViewBag.ListProjects = new SelectList(listProjectViewModels, "Id", "Title");

            List<UserViewModel> listUserViewModels = _userService.ListUsers().ToList();
            ViewBag.ListUsers = new SelectList(listUserViewModels, "Id", "Name");


            var listTaskStatusEnum = Enum.GetValues(typeof(TaskStatusEnum))
               .Cast<TaskStatusEnum>()
               .Select(ec => new 
                   {
                       Id = (int)ec,
                       Name = ec.ToString()
                   })
                   .ToList();

            SelectList listStatus = new SelectList(listTaskStatusEnum, "Id", "Name");
            ViewBag.ListTaskStatus = listStatus;

        }


        [HttpPost]
        public IActionResult InsertUpdate(TaskViewModel model)
        {
            _taskService.InsertUpdate(model);

            ViewBag.Mensagem = "Registered successfully!";
            LoadViewBags();
            return View("Form", new TaskViewModel());
        }

        [HttpGet]
        public IActionResult Delete(long id)
        {
            _taskService.Delete(id);
           
            return RedirectToAction("Index");
        }

    }
}
