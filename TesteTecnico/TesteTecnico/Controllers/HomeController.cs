﻿using Application.Interfaces;
using Application.Model;
using Domain.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using TesteTecnico.Models;

namespace TesteTecnico.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUserService _userService;

        public HomeController(ILogger<HomeController> logger, IUserService userService)
        {
            _logger = logger;
            _userService = userService;
        }

        public IActionResult Index()
        {
            HttpContext.Response.Cookies.Delete("UserId");
            HttpContext.Response.Cookies.Delete("UserName");

            return View("Login");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public IActionResult Login(string email, string password)
        {
            var user = IsValidUser(email, password);

            if (user != null)
            {
                HttpContext.Response.Cookies.Append("UserId", user.Id.ToString());
                HttpContext.Response.Cookies.Append("UserName", user.Name);

                return RedirectToAction("Index", "Project");
            }
            else
            {
                return Unauthorized();
            }
        }

     
        [HttpGet]
        public IActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateUser(UserViewModel model)
        {
            _userService.CreateUser(model);
           
            ViewBag.Mensagem = "Registered successfully!";
            return View();
        }


        private UserViewModel IsValidUser(string email, string password)
        {
           return _userService.Authenticate(email, password);
        }

    }
}