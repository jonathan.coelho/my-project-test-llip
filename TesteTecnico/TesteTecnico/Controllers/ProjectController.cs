﻿using Application.Interfaces;
using Application.Model;
using Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace TesteTecnico.Controllers
{
    public class ProjectController : Controller
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        public IActionResult Index()
        {
            var list = _projectService.ListProjects().ToList();
            return View(list);
        }

        [HttpGet]
        public IActionResult Form()
        {
            var model = new ProjectViewModel();
            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(long id)
        {
            var model = _projectService.GetById(id);
            return View("Form", model);
        }

        [HttpPost]
        public IActionResult InsertUpdate(ProjectViewModel model)
        {
            _projectService.InsertUpdate(model);

            ViewBag.Mensagem = "Registered successfully!";
            return View("Form", new ProjectViewModel());
        }

        [HttpGet]
        public IActionResult Delete(long id)
        {
            _projectService.Delete(id);

            return RedirectToAction("Index");
        }

    }
}
