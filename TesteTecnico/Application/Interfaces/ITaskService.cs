﻿using Application.Model;
using Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ITaskService
    {
        TaskViewModel GetById(long id);
        void InsertUpdate(TaskViewModel task);
        void Delete(long id);
        IEnumerable<TaskViewModel> ListTasks();
        IEnumerable<TaskViewModel> ListMyTasks(long userId);
        int CountUserTasks(long userId);
        IEnumerable<TaskViewModel> ListProjectTasks(long projectId);

    }
}
