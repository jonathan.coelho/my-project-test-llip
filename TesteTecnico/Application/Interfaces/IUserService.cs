﻿using Application.Model;
using Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IUserService 
    {
        UserViewModel GetById(long id);
        UserViewModel Authenticate(string email, string password);
        void CreateUser(UserViewModel user);
        IEnumerable<UserViewModel> ListUsers();
    }
}
