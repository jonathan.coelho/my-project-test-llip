﻿using Application.Model;
using Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IProjectService
    {
        ProjectViewModel GetById(long id);
        void InsertUpdate(ProjectViewModel project);
        void Delete(long id);
        IEnumerable<ProjectViewModel> ListProjects();
    }
}
