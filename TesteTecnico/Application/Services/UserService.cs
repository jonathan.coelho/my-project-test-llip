﻿using Application.Interfaces;
using Application.Model;
using AutoMapper;
using Domain.Entity;
using Infrastructure.Interfaces;
using Infrastructure.Repository;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly ITaskService _taskService;
        private readonly IMapper _mapper;

        public UserService(ITaskService taskService, IUserRepository repository, IMapper mapper)
        {
            _taskService = taskService;
            _userRepository = repository;
            _mapper = mapper;
        }

        public UserViewModel Authenticate(string email, string password)
        {
            return _mapper.Map<User, UserViewModel>(_userRepository.Authenticate(email, password));
        }

        public void CreateUser(UserViewModel user)
        {
            _userRepository.Insert(new User() { Name = user.Name, Email = user.Email, Password = user.Password });
            _userRepository.SaveChanges();
        }

        public UserViewModel GetById(long id)
        {
            return _mapper.Map<User, UserViewModel>(_userRepository.GetById(id));
        }

        public IEnumerable<UserViewModel> ListUsers()
        {
            var listUser = _mapper.Map<IEnumerable<User>, IEnumerable<UserViewModel>>(_userRepository.GetAll());

            foreach(var user in listUser)
            {
                user.TotalTask = _taskService.CountUserTasks(user.Id);
            }

            return listUser;
        }
    }
}
