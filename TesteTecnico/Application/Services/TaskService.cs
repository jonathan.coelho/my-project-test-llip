﻿using Application.Interfaces;
using Application.Model;
using AutoMapper;
using Domain.Entity;
using Infrastructure.Interfaces;
using Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class TaskService : ITaskService
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IMapper _mapper;

        public TaskService(ITaskRepository repository, IMapper mapper)
        {
            _taskRepository = repository;
            _mapper = mapper;
        }

        public TaskViewModel GetById(long id)
        {
            return _mapper.Map<TTask, TaskViewModel>(_taskRepository.GetById(id));
        }

        public void InsertUpdate(TaskViewModel task)
        {
            if (task.Id > 0)
                _taskRepository.Update(_mapper.Map<TaskViewModel, TTask>(task));
            else
                _taskRepository.Insert(_mapper.Map<TaskViewModel, TTask>(task));

            _taskRepository.SaveChanges();
        }

        public void Delete(long id)
        {
            var ttask = _taskRepository.GetById(id);
            _taskRepository.Remove(ttask);
            _taskRepository.SaveChanges();
        }

        public IEnumerable<TaskViewModel> ListTasks()
        {
            return _mapper.Map<IEnumerable<TTask>, IEnumerable<TaskViewModel>>(_taskRepository.GetAll());
        }

        public IEnumerable<TaskViewModel> ListMyTasks(long userId)
        {
            return _mapper.Map<IEnumerable<TTask>, IEnumerable<TaskViewModel>>(_taskRepository.GetTasksUser(userId));
        }

        public IEnumerable<TaskViewModel> ListProjectTasks(long projectId)
        {
            return _mapper.Map<IEnumerable<TTask>, IEnumerable<TaskViewModel>>(_taskRepository.GetAllTasksProject(projectId));
        }

        public int CountUserTasks(long userId)
        {
            return _taskRepository.CountUserTask(userId);
        }
    }
}
