﻿using Application.Interfaces;
using Application.Model;
using AutoMapper;
using Domain.Entity;
using Infrastructure.Interfaces;
using Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ProjectService : IProjectService
    {
        private readonly ITaskService _taskService;
        private readonly IProjectRepository _projectRepository;
        
        private readonly IMapper _mapper;

        public ProjectService(ITaskService taskService, IProjectRepository repository, IMapper mapper)
        {
            _taskService = taskService;
            _projectRepository = repository;
            _mapper = mapper;
        }

        public void InsertUpdate(ProjectViewModel project)
        {
            if(project.Id > 0)
                _projectRepository.Update(_mapper.Map<ProjectViewModel, Project>(project));
            else
                _projectRepository.Insert(_mapper.Map<ProjectViewModel, Project>(project));

            _projectRepository.SaveChanges();
        }

        public IEnumerable<ProjectViewModel> ListProjects()
        {
            return _mapper.Map<IEnumerable<Project>, IEnumerable<ProjectViewModel>>(_projectRepository.GetAll());
        }

        public ProjectViewModel GetById(long id)
        {
            return _mapper.Map<Project, ProjectViewModel>(_projectRepository.GetById(id));
        }

        public void Delete(long id)
        {
            if (id > 0)
            {
                var listTasks = _taskService.ListProjectTasks(id).ToList();
                foreach(var task in listTasks)
                {
                    _taskService.Delete(task.Id);
                }

                var project = _projectRepository.GetById(id);
                _projectRepository.Remove(project);
                _projectRepository.SaveChanges();
            }
        }
    }
}
