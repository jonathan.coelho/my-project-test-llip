﻿using Domain.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Model
{
    public class TaskViewModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string? Description { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public long ProjectId { get; set; }
        public ProjectViewModel Project { get; set; }
        public long? UserId { get; set; }
        public UserViewModel User { get; set; }
        public int? StatusId { get; set; }
    }
}
