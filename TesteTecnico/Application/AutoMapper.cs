﻿using Application.Model;
using AutoMapper;
using Domain.Entity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public class AutoMapper : Profile
    {
        public AutoMapper()
        {
            CreateMap<User, UserViewModel>();
            CreateMap<Project, ProjectViewModel>();
            CreateMap<TTask, TaskViewModel>();

            CreateMap<UserViewModel, User>();
            CreateMap<ProjectViewModel, Project>();
            CreateMap<TaskViewModel, TTask>();

        }
    }
}
