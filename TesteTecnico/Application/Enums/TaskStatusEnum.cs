﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Application.Enums
{
    public enum TaskStatusEnum
    {
        [EnumMember(Value = "1")]
        Pending = 1,

        [EnumMember(Value = "2")]
        InProgress = 2,

        [EnumMember(Value = "3")]
        Completed = 3
    }
}
