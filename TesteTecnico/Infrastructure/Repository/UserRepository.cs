﻿using Domain.Entity;
using Infrastructure.Context;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private DbSet<User> users;

        public UserRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
            users = applicationDbContext.Set<User>();
        }

        public User Authenticate(string email, string password)
        {
            return this.users.FirstOrDefault(x => x.Email.Equals(email) && x.Password.Equals(password));
        }

        public IEnumerable<User> GetAll()
        {
            return this.users;
        }

        public User GetById(long id)
        {
            return this.users.Find(id);
        }

        public void Insert(User entity)
        {
            this.users.Add(entity);
        }

        public void Remove(User entity)
        {
            throw new NotImplementedException();
        }

        public int SaveChanges()
        {
            return _applicationDbContext.SaveChanges();
        }

        public void Update(User entity)
        {
            throw new NotImplementedException();
        }
    }
}
