﻿using Domain.Entity;
using Infrastructure.Context;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private DbSet<Project> projects;

        public ProjectRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
            projects = applicationDbContext.Set<Project>();
        }

        public IEnumerable<Project> GetAll()
        {
            return this.projects;
        }

        public Project GetById(long id)
        {
            return this.projects.Find(id);
        }

        public void Insert(Project entity)
        {
            this.projects.Add(entity);
        }

        public void Update(Project entity)
        {
            this.projects.Update(entity);
        }

        public void Remove(Project entity)
        {
            this.projects.Remove(entity);
        }

        public int SaveChanges()
        {
            return _applicationDbContext.SaveChanges();
        }

       
    }
}
