﻿using Domain.Entity;
using Infrastructure.Context;
using Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class TaskRepository : ITaskRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private DbSet<TTask> tasks;

        public TaskRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
            tasks = applicationDbContext.Set<TTask>();
        }

        public IEnumerable<TTask> GetAll()
        {
            return this.tasks
                .Include(x => x.Project)
                .Include(x => x.User);
        }

        public TTask GetById(long id)
        {
            return this.tasks
                .Include(x => x.Project)
                .Include(x => x.User)
                .FirstOrDefault(x => x.Id == id);
        }

        public void Insert(TTask entity)
        {
            this.tasks.Add(entity);
        }

        public void Update(TTask entity)
        {
            this.tasks.Update(entity);
        }

        public void Remove(TTask entity)
        {
            this.tasks.Remove(entity);
        }

        public int SaveChanges()
        {
            return _applicationDbContext.SaveChanges();
        }

        public IEnumerable<TTask> GetTasksUser(long userId)
        {
            return this.tasks
                .Include(x => x.Project)
                .Include(x => x.User)
                .Where(x => x.UserId == userId);
        }

        public int CountUserTask(long userId)
        {
            return this.tasks
                .Include(x => x.Project)
                .Include(x => x.User)
                .Count(x => x.UserId == userId);
        }

        public IEnumerable<TTask> GetAllTasksProject(long projectId)
        {
            return this.tasks
                .Include(x => x.Project)
                .Include(x => x.User)
                .Where(x => x.ProjectId == projectId);
        }

    }
}
