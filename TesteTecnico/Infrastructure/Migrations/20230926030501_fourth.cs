﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class fourth : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdStatus",
                table: "TTasks");

            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "TTasks",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "TTasks");

            migrationBuilder.AddColumn<long>(
                name: "IdStatus",
                table: "TTasks",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);
        }
    }
}
