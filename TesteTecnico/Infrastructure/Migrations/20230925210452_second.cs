﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class second : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Titulo",
                table: "Projects",
                newName: "Title");

            migrationBuilder.RenameColumn(
                name: "Descricao",
                table: "Projects",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "DataCriacao",
                table: "Projects",
                newName: "CreateDate");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Title",
                table: "Projects",
                newName: "Titulo");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "Projects",
                newName: "Descricao");

            migrationBuilder.RenameColumn(
                name: "CreateDate",
                table: "Projects",
                newName: "DataCriacao");
        }
    }
}
