﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class third : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CompletionDate",
                table: "TTasks",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "TTasks",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "TTasks",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "IdStatus",
                table: "TTasks",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "ProjectId",
                table: "TTasks",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "TTasks",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "TTasks",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_TTasks_ProjectId",
                table: "TTasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_TTasks_UserId",
                table: "TTasks",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_TTasks_Projects_ProjectId",
                table: "TTasks",
                column: "ProjectId",
                principalTable: "Projects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TTasks_Users_UserId",
                table: "TTasks",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TTasks_Projects_ProjectId",
                table: "TTasks");

            migrationBuilder.DropForeignKey(
                name: "FK_TTasks_Users_UserId",
                table: "TTasks");

            migrationBuilder.DropIndex(
                name: "IX_TTasks_ProjectId",
                table: "TTasks");

            migrationBuilder.DropIndex(
                name: "IX_TTasks_UserId",
                table: "TTasks");

            migrationBuilder.DropColumn(
                name: "CompletionDate",
                table: "TTasks");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "TTasks");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "TTasks");

            migrationBuilder.DropColumn(
                name: "IdStatus",
                table: "TTasks");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "TTasks");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "TTasks");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "TTasks");
        }
    }
}
