﻿using DDD.Infra.Data.Repository;
using Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interfaces
{
    public interface IUserRepository
    {
        User GetById(long id);
        User Authenticate(string email, string password);
        IEnumerable<User> GetAll();
        void Insert(User entity);
        void Update(User entity);
        void Remove(User entity);
        int SaveChanges();
    }
}
