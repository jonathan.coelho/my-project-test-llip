﻿using DDD.Infra.Data.Repository;
using Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interfaces
{
    public interface IProjectRepository
    {
        Project GetById(long id);
        IEnumerable<Project> GetAll();
        void Insert(Project entity);
        void Update(Project entity);
        void Remove(Project entity);
        int SaveChanges();
    }
}
