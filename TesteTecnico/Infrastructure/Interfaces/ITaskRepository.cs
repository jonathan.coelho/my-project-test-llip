﻿using Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Interfaces
{
    public interface ITaskRepository
    {
        TTask GetById(long id);
        IEnumerable<TTask> GetAll();
        IEnumerable<TTask> GetTasksUser(long userId);
        int CountUserTask(long userId);
        IEnumerable<TTask> GetAllTasksProject(long projectId);
        void Insert(TTask entity);
        void Update(TTask entity);
        void Remove(TTask entity);
        int SaveChanges();
    }
}
